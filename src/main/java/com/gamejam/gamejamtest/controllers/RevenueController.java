package com.gamejam.gamejamtest.controllers;

import com.gamejam.gamejamtest.entities.Campaign;
import com.gamejam.gamejamtest.services.RevenueService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class RevenueController {

    private final RevenueService revenueService;

    @GetMapping("/revenue")
    public Double calculateRevenue(
            @RequestParam(value = "project_day") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate projectDay
    ) {
        return revenueService.calculateRevenue(projectDay);
    }

}
