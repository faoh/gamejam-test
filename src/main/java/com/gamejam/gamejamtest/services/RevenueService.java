package com.gamejam.gamejamtest.services;

import com.gamejam.gamejamtest.entities.Campaign;
import com.gamejam.gamejamtest.repositories.CampaignRepository;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.jni.Local;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RevenueService {

    private final CampaignRepository campaignRepository;

    public Double calculateRevenue(LocalDate projectDay) {
        Campaign campaign = campaignRepository.getCampaign("C1");

        Map<Long, Double> totalDecreaseRateAfterDayNth = new HashMap<>();
        Map<Long, Long> totalDayPreComputed = new HashMap<>();

        // pre-compute
        for (Map.Entry<LocalDate, Map<LocalDate, Double>> revenueByDay : campaign.getRevenues().entrySet()) {
            LocalDate firstDay = revenueByDay.getKey();
            for (Map.Entry<LocalDate, Double> revenue : revenueByDay.getValue().entrySet()) {
                long numOfDayAfter = ChronoUnit.DAYS.between(firstDay, revenue.getKey());
                if (numOfDayAfter > 0) {
                    Double revenueBefore = revenueByDay.getValue().get(revenue.getKey().minusDays(1L));
                    Double decreaseRevenue = (revenueBefore - revenue.getValue()) / revenueBefore;
                    if (totalDecreaseRateAfterDayNth.containsKey(numOfDayAfter)) {
                        totalDecreaseRateAfterDayNth.put(numOfDayAfter, totalDecreaseRateAfterDayNth.get(numOfDayAfter) + decreaseRevenue);
                    } else {
                        totalDecreaseRateAfterDayNth.put(numOfDayAfter, decreaseRevenue);
                    }
                    if (totalDayPreComputed.containsKey(numOfDayAfter)) {
                        totalDayPreComputed.put(numOfDayAfter, totalDayPreComputed.get(numOfDayAfter) + 1);
                    } else {
                        totalDayPreComputed.put(numOfDayAfter, 1L);
                    }
                }
            }
        }
        Map<Long, Double> avgDecreaseRateAfterDayNth = totalDecreaseRateAfterDayNth.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue() / totalDayPreComputed.get(e.getKey())));

        double nextDecreaseRate;
        do {
            long maxDayAfter = Collections.max(avgDecreaseRateAfterDayNth.entrySet(), Map.Entry.comparingByKey()).getKey() + 1;
            nextDecreaseRate = predict(maxDayAfter);
            if (nextDecreaseRate > 0) {
                avgDecreaseRateAfterDayNth.put(maxDayAfter, nextDecreaseRate);
            }
        } while (nextDecreaseRate > 0);

        Double totalSpends = campaign.getSpends().values().stream()
                .mapToDouble(Double::doubleValue)
                .sum();

        double totalRevenue = 0.0;
        for (Map.Entry<LocalDate, Map<LocalDate, Double>> revenueByDay : campaign.getRevenues().entrySet()) {
            long totalDay = ChronoUnit.DAYS.between(revenueByDay.getKey(), projectDay) + 1;
            for (int day = 1; day < totalDay; day++) {
                if (!revenueByDay.getValue().containsKey(revenueByDay.getKey().plusDays(day))) {
                    if (avgDecreaseRateAfterDayNth.containsKey((long) day)) {
                        revenueByDay.getValue().put(revenueByDay.getKey().plusDays(day),
                                revenueByDay.getValue().get(revenueByDay.getKey().plusDays(day - 1))
                                        - revenueByDay.getValue().get(revenueByDay.getKey().plusDays(day - 1)) * avgDecreaseRateAfterDayNth.get((long) day));
                    } else {
                        revenueByDay.getValue().put(revenueByDay.getKey().plusDays(day),
                                revenueByDay.getValue().get(revenueByDay.getKey().plusDays(day - 1)));

                    }
                }
            }
            for (Map.Entry<LocalDate, Double> revenue : revenueByDay.getValue().entrySet()) {
                if (revenue.getKey().isAfter(projectDay)) {
                    continue;
                }
                totalRevenue += revenue.getValue();
            }
        }

        return totalRevenue;
    }

    private double predict(long numOfDayAfterFirstDay) {
        // TODO: using linear regression to compute this
        return -0.274 * numOfDayAfterFirstDay + 1.1;
    }

}
