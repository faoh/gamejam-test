package com.gamejam.gamejamtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GamejamTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(GamejamTestApplication.class, args);
    }

}
