package com.gamejam.gamejamtest.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Campaign {

    private String code;
    private LocalDate startDate;
    private LocalDate endDate;
    private Map<LocalDate, Double> spends;
    private Map<LocalDate, Long> installs;
    private Map<LocalDate, Map<LocalDate, Double>> revenues;

}
