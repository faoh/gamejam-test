package com.gamejam.gamejamtest.repositories;

import com.gamejam.gamejamtest.entities.Campaign;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Repository
@RequiredArgsConstructor
public class CampaignRepository {

    public Campaign getCampaign(String code) {
        // TODO: read from DB
        return Campaign.builder()
                .code(code)
                .startDate(LocalDate.of(2020, 3, 13))
                .endDate(LocalDate.of(2020, 3, 16))
                .installs(Map.of(
                        LocalDate.of(2020, 3, 13), 2400L,
                        LocalDate.of(2020, 3, 14), 2500L,
                        LocalDate.of(2020, 3, 15), 2600L,
                        LocalDate.of(2020, 3, 16), 2600L
                ))
                .spends(Map.of(
                        LocalDate.of(2020, 3, 13), 745.0,
                        LocalDate.of(2020, 3, 14), 794.0,
                        LocalDate.of(2020, 3, 15), 840.0,
                        LocalDate.of(2020, 3, 16), 840.0
                ))
                .revenues(new HashMap<>(Map.of(
                        LocalDate.of(2020, 3, 13), new HashMap<>(Map.of(
                                LocalDate.of(2020, 3, 13), 408.06,
                                LocalDate.of(2020, 3, 14), 83.09,
                                LocalDate.of(2020, 3, 15), 35.68,
                                LocalDate.of(2020, 3, 16), 26.87
                        )),
                        LocalDate.of(2020, 3, 14), new HashMap<>(Map.of(
                                LocalDate.of(2020, 3, 14), 408.27,
                                LocalDate.of(2020, 3, 15), 98.78,
                                LocalDate.of(2020, 3, 16), 36.67
                        )),
                        LocalDate.of(2020, 3, 15), new HashMap<>(Map.of(
                                LocalDate.of(2020, 3, 15), 416.00,
                                LocalDate.of(2020, 3, 16), 85.29
                        )),
                        LocalDate.of(2020, 3, 16), new HashMap<>(Map.of(
                                LocalDate.of(2020, 3, 16), 449.47
                        ))
                )))
                .build();
    }

}
